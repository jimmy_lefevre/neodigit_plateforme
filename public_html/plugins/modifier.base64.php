<?php
/**
 * Smarty plugin
 *
 * @package    Smarty
 * @author JLEFEVRE
 */

function smarty_modifier_base64($path) {
	$url = __DIR__.'/../public_html/'.$path;
	error_log($url);
	$type = pathinfo($path, PATHINFO_EXTENSION);
	$data = file_get_contents($url);
	$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
    return $base64;
}