<?php
/**
 * Smarty plugin
 *
 * @package    Smarty
 * @author JLEFEVRE
 */

function smarty_modifier_urlize($str) {
	$clean = (stripslashes($str));
	$clean = Normalizer::normalize($clean, Normalizer::FORM_D);
	$clean = str_replace(['&quot;', '&amp;', '&rsquo;'], '', $clean);
    $clean = preg_replace("/&(.)(acute|grave|circ|uml|cedil|ring|tilde|slash);/", '\1', $clean);
    $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
    $clean = strtolower(trim($clean, '-'));
    $clean = preg_replace("/[\/_|+ -]+/", "-", $clean);
    return $clean;
}