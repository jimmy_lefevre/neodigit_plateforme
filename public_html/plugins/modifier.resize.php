<?php
/**
 * Smarty plugin
 *
 * @package    Smarty
 * @author JLEFEVRE
 */


 function smarty_modifier_resize($str, $width=200, $height=150, $watermark_enabled=true)
 {
	$file = pathinfo($str)['filename'].'-'.$width.'x'.$height;

	if ($watermark_enabled) {
		$file .= '-wd';		
	}

	$file .= '.'.pathinfo($str)['extension'];
	$dir = dirname($str);

	$thumb = str_replace($file, $file, $str);

	$thumb_dir = __DIR__.'/../public_html/images/';
	$thumb_file =  $thumb_dir.$file;
	$local_file = __DIR__.'/../public_html'.$str;

	if (!file_exists($thumb_file)) {
		if (!file_exists($thumb_dir)) {
		    mkdir($thumb_dir, 0777, true);
		}
		generate_thumb($local_file, $thumb_file, $width, $height, $watermark_enabled);
		//copy($thumb_file, $thumb_dir.'_'.$file);

		$factory = new \ImageOptimizer\OptimizerFactory();
		switch(pathinfo($str)['extension']) {
			case 'jpg': 
			case 'jpeg': 
		    $jpgOptimizer = $factory->get('jpegoptim');
		    $jpgOptimizer->optimize($thumb_file);
			break;
			case 'png':
		    $pngOptimizer = $factory->get('png');
		    $pngOptimizer->optimize($thumb_file);
			break;
		}

	}


	return str_replace(__DIR__.'/../public_html', '', $thumb_dir).$file;
 }

 function generate_thumb($from, $to, $w, $h, $watermark_enabled=false) {
	$im = new imagick( $from );
	$im->scaleImage( $w, $h);

	$settings = json_decode(file_get_contents(__DIR__.'/../configs/settings.json'));
	
	$watermark_path = $settings->site->watermark;

 	if ($watermark_enabled) {
 		$_wd = $im->getImageGeometry();
		$_ww = $_wd['width']; 
		$_wh = $_wd['height'];

	 	$watermark  = new imagick( __DIR__.'/../public_html'.$watermark_path);
	 	$watermark->scaleImage($_ww*.25, $_wh*.25, true);
	 	$watermark->evaluateImage(Imagick::EVALUATE_DIVIDE, 5, Imagick::CHANNEL_ALPHA);
		$watermark->rotateImage(new ImagickPixel('#00000000'), -25);

		$wd = $watermark->getImageGeometry();
		$ww = $wd['width']; 
		$wh = $wd['height'];

		$im->compositeImage($watermark, imagick::COMPOSITE_OVER, ($_ww) - ($ww) , ($_wh) - ($wh) );
 	}

	$im->writeImage($to);
 }