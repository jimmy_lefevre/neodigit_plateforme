<?php
/**
 * Smarty plugin
 *
 * @author JLEFEVRE
 */

require_once __DIR__.'/../vendor/autoload.php';

function smarty_function_favicon($params, &$smarty)
{
	$target =  __DIR__.'/../public_html/favicon.ico';
	if (!file_exists($target)) {
		$from =  __DIR__.'/../public_html'.$params['src'];
		ico($from, $target);
	}
	return '/favicon.ico';
}

function ico($from, $to) {
	$src_img = new Imagick($from);
	$icon = new Imagick();
	$icon->setFormat("ico");

	$geo=$src_img->getImageGeometry();

	$size_w=$geo['width'];
	$size_h=$geo['height']; 

	if (128/$size_w*$size_h>128) {
	  $src_img->scaleImage(128,0);
	} else {
	  $src_img->scaleImage(0,128); 
	} 

	$src_img->cropImage(128, 128, 0, 0);

	$clone = new Imagick($from);
	$clone->scaleImage(128,0);    
	$icon->addImage($clone);

	$clone->scaleImage(64,0);            
	$icon->addImage($clone);

	$clone->scaleImage(32,0);            
	$icon->addImage($clone);

	$clone->scaleImage(16,0);            
	$icon->addImage($clone);


	$icon->writeImages($to, true);

	$src_img->destroy(); 
	$icon->destroy(); 
	$clone->destroy(); 
 }