<?php
/**
 * Smarty plugin
 *
 * @package    Smarty
 * @author JLEFEVRE
 */

function smarty_modifier_maketimestamp($str) {
	//25.2.2017
	
	$items = explode('.', $str);
	
	$month = $items[1];
	$day = $items[0];
	$year = $items[2];

    return mktime(0, 0, 0, $month, $day, $year);
}